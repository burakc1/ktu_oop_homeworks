#include "opOverClass.h"


opOverClass::opOverClass()
{
    a = 10;
    b = 10;
    //ctor
}

opOverClass::opOverClass(int a_in, int b_in){
     a = a_in;
     b = b_in;
}

opOverClass opOverClass::operator+(opOverClass o_in){
   opOverClass temp;
   temp.a = this->a + o_in.a;
   temp.b = this->b + o_in.b;
   return temp;
}
int opOverClass::get_a(){
  return a;
}
int opOverClass::get_b(){
  return b;
}
opOverClass::~opOverClass()
{
    //dtor
}
