#ifndef OPOVERCLASS_H
#define OPOVERCLASS_H

class opOverClass
{
 public :
   opOverClass();
   opOverClass(int a_in, int b_in);
   int get_a();
   int get_b();
   opOverClass operator+(opOverClass o_in);

   virtual ~opOverClass();


   private:
   int a;
   int b;
};
#endif // OPOVERCLASS_H
