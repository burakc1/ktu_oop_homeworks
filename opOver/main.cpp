#include <iostream>
#include "opOverClass.h"
using namespace std;

int main()
{
    opOverClass a(10,20);
    opOverClass b(20,30);
    opOverClass c = a + b;

    cout << "Hello world! " << c.get_a() <<" " << c.get_b() << endl;
    return 0;
}
